# Alpha FX - Pin Code Test

## Introduction

The front-end code for the Alpha FX test Pin Code app. Built in [React](https://reactjs.org/).

## Requirements

- [Node](https://nodejs.org/en/).
- [npm](https://www.npmjs.com/package/npm).

## Getting Started

**1. Clone Git Repo.**

```
$ git clone https://kkbokyere@bitbucket.org/kkbokyere/pin-code-test.git
```

**2. Install Dependencies.**

Once that's all done, cd into the star-wars-trump directory and install the depedencies:

```
$ cd pin-code-test/web
$ yarn install
```

**3. Run Application.**

Once the node modules have all been installed and npm has done it's thing, that's it. To open up a local development environment, run:

```
$ yarn start
```

Once the server is up and running, navigate to [localhost:3000](http://localhost:3000).

## Testing

[Jest](https://jestjs.io/) is the test runner used, with [React Testing Library](https://testing-library.com/docs/react-testing-library/) is testing library used for testing components. To run test use the following command:

```
$ yarn test
```

## Deployment

No CI/CD pipeline at the moment.

# Tools Used

- [React](https://reactjs.org/)
- [Create React App](https://create-react-app.dev/)
- [Redux](https://redux.js.org)
- [CSS Modules](https://github.com/css-modules/css-modules)
- [Webpack](https://webpack.js.org/)

# Improvements / Retrospective Review

- Would have used Cypress for E2E testing
- create a better Error handler
- implement typescript to better type definitions
- Would have considered creating a proper [SMACSS](http://smacss.com/) architecture for base CSS styles such as layout. 
- better styling
- would have functionality to focus on key when pressed down on keyboard
- potentially consider using useCallback hook to handle better performance on keydown
- couldn't get test working for keyboard press
- hide error message after some time
