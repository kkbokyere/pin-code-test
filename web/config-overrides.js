const { override, addWebpackAlias } = require('customize-cra');
const path = require('path');

module.exports = override(
    addWebpackAlias({
        '@root': path.resolve(__dirname, './src'),
        '@actions': path.resolve(__dirname, './src/actions'),
        '@containers': path.resolve(__dirname, './src/containers'),
        '@components': path.resolve(__dirname, './src/components'),
        '@constants': path.resolve(__dirname, './src/constants'),
        '@reducers': path.resolve(__dirname, './src/reducers'),
        '@styles': path.resolve(__dirname, './src/styles')
    })
);
