import { connect } from 'react-redux'
import App from "./App";
import {incrementAttempts, decrementAttempts, clearAttempts} from "@actions/attempts";
const mapStateToProps = ({ attempts }) => ({
    attempts
});

const mapDispatchToProps = dispatch => ({
    incrementAttempts: () => dispatch(incrementAttempts()),
    decrementAttempts: () => dispatch(decrementAttempts()),
    clearAttempts: () => dispatch(clearAttempts())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
