import React, { useEffect, useState} from 'react';
import cx from 'classnames'
import '@styles/App.scss';
import Layout from "@components/Layout";
import Keypad from "@components/Keypad";
import Combination from "../Combination";
import {CORRECT_COMBINATION} from "../../constants/general";

function App({ incrementAttempts, clearAttempts, attempts}) {
  const combinationLength = CORRECT_COMBINATION.toString().length;
  const [ combination, updateCombination ] = useState([]);
  const [ errorMessage, setErrorMessage ] = useState(null);
  const [ isLocked, setLockState ] = useState(true);
  const [ disableKeypad, setDisableKeyPad ] = useState(false);
  const [ disableUnlockButton, setDisableUnlockButton ] = useState(true);
  const errorClasses = cx({
    'error':!!errorMessage
  });
  const handleKeyDown = (event) => {
    const pressedKey = Number(event.key);
    if(!isNaN(pressedKey)) {
      handleUpdateCombination()
    }
    if (event.keyCode === 13) {
      handleOnClickUnlock()
    }
  };

  const checkCombinationMatch = () => {
    return Number(combination.join("")) === CORRECT_COMBINATION
  };

  const handleUpdateCombination = (key) => {
    if(combination.length < combinationLength){
      updateCombination([...combination, key])
    }
  };

  const handleOnClear = () => {
    updateCombination([]);
    setErrorMessage(null);
    setLockState(true);
  };

  const handleOnClickUnlock = () => {
      if(checkCombinationMatch()) {
        setLockState(false);
        setErrorMessage(null);
        clearAttempts();
      } else {
        setErrorMessage("Incorrect entry");

        setTimeout(() => {
          setErrorMessage(null);
        }, 1000);
        updateCombination([]);
        setLockState(true);
        incrementAttempts()
      }
  };
  useEffect(() => {
    if(attempts.count === attempts.limit) {
      setErrorMessage("Too many incorrect attempts");
      setDisableKeyPad(true)
    }
    setDisableUnlockButton(combination.length !== combinationLength);
    document.addEventListener("keydown", handleKeyDown);
    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    }
  }, [attempts, combination, combinationLength]);
  return (
    <div className="App">
      <Layout>
        <span data-testid="error-message" className={errorClasses}>{errorMessage}</span>
        <h1 data-testid="lock-state">{isLocked ? "Locked":"Unlocked"}</h1>
        <h2>Attempts: {attempts.count}</h2>
        <Combination combination={combination}/>
        <Keypad
            isUnlockDisabled={disableUnlockButton}
            isDisabled={disableKeypad}
            handleOnClickKey={(e) => handleUpdateCombination(e.currentTarget.value)}
            handleOnClickClear={handleOnClear}
            handleOnClickUnlock={handleOnClickUnlock}
        />
      </Layout>
    </div>
  );
}

export default App;
