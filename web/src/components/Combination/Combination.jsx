import React from 'react';
import styles from './Combination.module.scss'
import PropTypes from 'prop-types';
const Combination = ({combination = []}) => {
    return (
        <div data-testid="combination" className={styles.container}>
            {combination.map(() => '*')}
        </div>
    );
};

Combination.propTypes = {
    combination: PropTypes.array
};

export default Combination;
