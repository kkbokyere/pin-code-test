import React from 'react';
import Key from "@components/Key";
import styles from './Keypad.module.scss'
import PropTypes from 'prop-types'
const Keypad = ({ isUnlockDisabled, handleOnClickClear, handleOnClickKey, handleOnClickUnlock, isDisabled }) => {
    const digitKeys = [1,2,3,4,5,6,7,8,9,0];
    if (isDisabled) {
        return null
    }
    return (
        <div className={styles.keypad} data-testid="keypad">
            {digitKeys.map((digit, i) => <Key key={i} handleOnClick={handleOnClickKey} value={digit}>{digit}</Key>)}
            <Key handleOnClick={handleOnClickClear}>Clear</Key>
            <Key disabled={isUnlockDisabled} handleOnClick={handleOnClickUnlock}>Unlock</Key>
        </div>
    );
};

Keypad.propTypes = {
    isDisabled: PropTypes.bool,
    isUnlockDisabled: PropTypes.bool,
    clearAttempts: PropTypes.func,
    handleOnClickUnlock: PropTypes.func,
    handleOnClickClear: PropTypes.func,
};

export default Keypad;
