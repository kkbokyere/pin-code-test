import React from 'react';
import styles from './Key.module.scss'
import PropTypes from "prop-types";
const Key = ({ children, handleOnClick, value, ...props}) => {
    return (
        <button {...props} onClick={handleOnClick} value={value} className={styles.key}>
            {children}
        </button>
    );
};
Key.propTypes = {
    children: PropTypes.any,
    value: PropTypes.any,
    handleOnClick: PropTypes.func
};
export default Key;
