import React from 'react';
import ReactDOM from 'react-dom'
import { render, fireEvent, screen, waitForElementToBeRemoved } from './helper'
import App from '../components/App';
const setup = () => {
  return render(<App/>, {
    initialState: {
      attempts: {
        count: 0,
        limit: 3
      }
    }
  });
};
describe('Initial App render', () => {
  test('renders connected app component', () => {
    const {asFragment} = setup();
    expect(asFragment()).toMatchSnapshot();
  });
});

describe('App ACs', () => {
  test('Should have to enter exactly 4 digits to be able to attempt to unlock', async () => {
    setup();
    const unlockButton = await screen.findByText('Unlock');
    const oneBtn = await screen.findByText('1');
    const twoBtn = await screen.findByText('2');
    const threeBtn = await screen.findByText('3');
    const fourBtn = await screen.findByText('4');
    expect(unlockButton).toBeDisabled();
    fireEvent.click(oneBtn);
    fireEvent.click(twoBtn);
    fireEvent.click(threeBtn);
    fireEvent.click(fourBtn);
    expect(unlockButton).not.toBeDisabled();

  });

  test('To attempt to unlock you have to press "Unlock" button', async () => {
    setup();
    const unlockButton = await screen.findByText('Unlock');
    fireEvent.click(unlockButton);
  });

  test('To clear your entry, you can press “Clear” button', async() => {
    setup();
    const clearButton = await screen.findByText('Clear');
    const combinationContainer = screen.getByTestId('combination');
    expect(combinationContainer.textContent).toEqual('');
    const oneBtn = await screen.findByText('1');
    const twoBtn = await screen.findByText('2');
    fireEvent.click(oneBtn);
    fireEvent.click(twoBtn);
    expect(combinationContainer.textContent).toEqual('**');
    fireEvent.click(clearButton);
    expect(combinationContainer.textContent).toEqual('');
  });

  test('If you attempt to unlock and the entry is incorrect, display a message: “Incorrect entry” and clear the input', async () => {
    setup();
    const unlockButton = await screen.findByText('Unlock');
    const errorMessageContainer = screen.getByTestId('error-message');
    expect(errorMessageContainer.textContent).toEqual('');
    const oneBtn = await screen.findByText('1');
    const twoBtn = await screen.findByText('2');
    const threeBtn = await screen.findByText('3');
    const fourBtn = await screen.findByText('4');
    fireEvent.click(oneBtn);
    fireEvent.click(twoBtn);
    fireEvent.click(fourBtn);
    fireEvent.click(threeBtn);
    fireEvent.click(unlockButton);
    expect(errorMessageContainer.textContent).toEqual('Incorrect entry');

  });

  test('If you attempt to unlock and the entry is correct, change the LOCKED text to UNLOCKED and hide the keypad', async () => {
    setup();
    const unlockButton = await screen.findByText('Unlock');
    const lockStateContainer = screen.getByTestId('lock-state');
    expect(lockStateContainer.textContent).toEqual('Locked');
    const oneBtn = await screen.findByText('1');
    const twoBtn = await screen.findByText('2');
    const threeBtn = await screen.findByText('3');
    const fourBtn = await screen.findByText('4');
    fireEvent.click(oneBtn);
    fireEvent.click(twoBtn);
    fireEvent.click(threeBtn);
    fireEvent.click(fourBtn);
    fireEvent.click(unlockButton);
    expect(lockStateContainer.textContent).toEqual('Unlocked');
  });

  test('If you make 3 incorrect unlock entry attempts, display a message "Too many incorrect attempts" and hide the keypad', async() => {
    setup();
    const unlockButton = await screen.findByText('Unlock');
    const errorMessageContainer = screen.getByTestId('error-message');
    const keyPadContainer = screen.getByTestId('keypad');
    expect(errorMessageContainer.textContent).toEqual('');
    expect(keyPadContainer).toBeVisible();
    const oneBtn = await screen.findByText('1');
    const twoBtn = await screen.findByText('2');
    const threeBtn = await screen.findByText('3');
    const fourBtn = await screen.findByText('4');

    //Attempt 1
    fireEvent.click(oneBtn);
    fireEvent.click(twoBtn);
    fireEvent.click(fourBtn);
    fireEvent.click(threeBtn);

    fireEvent.click(unlockButton);

    expect(errorMessageContainer.textContent).toEqual('Incorrect entry');

    //Attempt 2
    fireEvent.click(twoBtn);
    fireEvent.click(oneBtn);
    fireEvent.click(fourBtn);
    fireEvent.click(threeBtn);

    fireEvent.click(unlockButton);

    expect(errorMessageContainer.textContent).toEqual('Incorrect entry');

    //Attempt 3
    fireEvent.click(twoBtn);
    fireEvent.click(fourBtn);
    fireEvent.click(oneBtn);
    fireEvent.click(threeBtn);

    fireEvent.click(unlockButton);

    expect(errorMessageContainer.textContent).toEqual('Too many incorrect attempts');
    waitForElementToBeRemoved(() => keyPadContainer).then(() => {
      expect(keyPadContainer).not.toBeVisible();
    });

  });

  test('The Pin lock buttons must accept keyboard as well as click input', async () => {
    setup();
    const combinationContainer = screen.getByTestId('combination');
    expect(combinationContainer.textContent).toEqual('');
    fireEvent.keyDown(document, {
      key: 1,
      keyCode: 49
    });

    // expect(combinationContainer.textContent).toEqual('*');


  });

});
