import React from 'react';
import { render, screen } from '../helper'
import Combination from "@components/Combination";
const setup = (props) => {
    return render(<Combination {...props}/>);
};
describe('Combination', () => {
    test('renders Combination component', () => {
        const {asFragment} = setup();
        expect(asFragment()).toMatchSnapshot();
    });
    test('renders Combination component text as astrix', async () => {
        setup({ combination: [1,2,3,4] });
        const combinationContainer = screen.getByTestId('combination');
        expect(combinationContainer.textContent).toEqual('****');
    });
});
