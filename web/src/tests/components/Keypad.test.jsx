import React from 'react';
import { render } from '../helper'
import Keypad from "../../components/Keypad";
const setup = () => {
    return render(<Keypad/>);
};
describe('Keypad', () => {
    test('renders Keypad component', () => {
        const {asFragment} = setup();
        expect(asFragment()).toMatchSnapshot();
    });
});
