import React from 'react';
import { render } from '../helper'
import Key from "@components/Key";
const setup = () => {
    return render(<Key/>);
};
describe('Key', () => {
    test('renders Key component', () => {
        const {asFragment} = setup();
        expect(asFragment()).toMatchSnapshot();
    });
});
