import types from "@constants/action-types";

export function incrementAttempts() {
    return {
        type: types.INCREMENT_ATTEMPTS
    }
}

export function decrementAttempts() {
    return {
        type: types.DECREMENT_ATTEMPTS
    }
}

export function clearAttempts() {
    return {
        type: types.CLEAR_ATTEMPTS
    }
}
