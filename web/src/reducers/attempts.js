import types from '@constants/action-types';

const initialState = {
    limit: 3,
    count: 0
};

export default (state = initialState, action) => {
    let { type } = action;
    switch (type) {
        case types.INCREMENT_ATTEMPTS:
            return {
                ...state,
                count: state.count+1
            };
        case types.DECREMENT_ATTEMPTS:
            return {
                ...state,
                count: state.count - 1
            };
            case types.CLEAR_ATTEMPTS:
            return initialState;
        default:
            return state
    }
}
